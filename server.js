/**
 * Module dependencies.
 */

var express = require('express')
    , http = require('http')
    , path = require('path')
    , cons = require('consolidate')
    , swig = require('swig')
    , appController = require('./lib/controller/controller.js');

var app = express(),
    debug = 'development' == app.get('env');

// all environments
app.set('port', process.env.PORT || 1337);
app.set('views', __dirname + '/views');
app.engine('tpl', cons.swig); // assign the swig engine to .tpl files
app.set('view engine', 'tpl'); // set .tpl as the default extension
app.use(express.favicon(__dirname + '/public/favicon.ico'));
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.compress()); // gzip content
app.use(require('less-middleware')({ src: __dirname + '/public' }));
app.use(express.static(path.join(__dirname, 'public')));

// initialize local variables that can be access by every template
app.locals({
  appName: 'OfficeFood',
  appUrl: 'officefood.hu',
  today: new Date()
});

swig.init({
    root: __dirname + '/views', // Tell swig where to look for templates when one extends another.
    allowErrors: true, // allows errors to be thrown and caught by express instead of suppressed by Swig
    cache: !debug // disable caching incase of development enviroment
});

// development only
if (debug) {
    app.use(express.errorHandler());
}

app.get('/favicon.ico', function(req, res) {
    res.render(200, '');
});
app.get('/admin', function(req, res) {
    res.render('admin/index', {'username' : 'foodAdmin'});
});
app.get('/', function(req, res) {
    appController.parseRequest(req,res);
});
app.get('/*', function(req, res) {
    appController.parseRequest(req,res);
});

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});