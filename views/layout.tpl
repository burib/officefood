<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>{% block title %}Hello {{appName}}{% endblock %}</title>
  {% block head %}
  <link rel="stylesheet" href="/css/base.css">
  <link rel="stylesheet" href="/css/layout.css">
  <link rel="stylesheet" href="/css/theme.css">
  {% endblock %}
</head>
<body>
  {% block content %}Hello {{appName}}{% endblock %}
  {% block jsFiles %}
  <script type="text/javascript" src="/js/app.core.js"></script>
  {% endblock %}
</body>
</html>