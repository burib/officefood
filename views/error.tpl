{% extends 'layout.tpl' %}

{% block title %}Error{% endblock %}
{% block head %}
{% parent %}
	<link rel="stylesheet" href="/css/error.css">
{% endblock %}

{% block content %}
	<h1>Error 404</h1>
  <h3>You've forgot to create your view file at : {{data.viewName}}</h3>
{% endblock %}