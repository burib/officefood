{% extends 'layout.tpl' %}
{% block title %}éttermek{% endblock %}
{% block content %}
    <h3>Officefood</h3>
    <h1>Napi menü - {{today|date('Y.m.d')}}</h1>
    {% for item in data.content %}
      {% if loop.first %}<ul class="restaurant-list">{% endif %}
      <li class="restaurant border">
        <div class="logo rounded border"></div>
        <h2 class="title">{{item.restaurant.name}}</h2>
        <a href="#" class="button info rounded">{{item.restaurant.address}}</a>
        {% for course in item.dishes %}
        <div class="course">
            <h4>{{course.name}}</h4>
            <div class="dish">
                {% for dish in course.content %}
                    <h5 class="bullet-item">{{dish.title}}</h5>
                {% endfor %}
            </div>
            <div class="price rounded">{{course.price.amount}}<span class="currency">{{course.price.currency}}</span></div>
        </div>
        {% endfor %}
      </li>
      {% if loop.last %}</ul>{% endif %}
    {% endfor %}

{% endblock %}