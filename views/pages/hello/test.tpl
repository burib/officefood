{% extends 'layout.tpl' %}

{% block title %}{% endblock %}
{% block head %}
{% parent %}
	{% if "css" in data %}
	<link rel="stylesheet" href="/css/{{data.css}}">
	{% endif %}
{% endblock %}

{% block content %}
	{{data.content}}
{% endblock %}