var fs = require('fs'),
    path = require('path'),
    cwd = process.cwd(),
    controllerPath = cwd + '\\controllers\\',
    viewPath = cwd + '\\views\\',
    viewFile,
    controllers = {},
    appController = {
        parseRequest: function (req, res) {
            var params = req.url;
                urlParts = params.split('/'),
                routes = {
                    'restaurants': 'helloController',
                    'ettermek': 'helloController',
                },
                controllerName = urlParts[1] && urlParts[1].toLowerCase()  + 'Controller' || "defaultController",
                methodName = null,
                self = this;


                if (urlParts[2]) {
                    methodName = urlParts[2].toLowerCase();
                }

                fs.exists(path.normalize(controllerPath + controllerName + '.js'), function (exists) {
                    self.createController.call(self, ( exists ? controllerName : 'defaultController' ), methodName, req, res);
                });

        },
        createController: function (controllerName, methodName, req, res) {
            var contructor = require(path.normalize(controllerPath + controllerName + '.js')),
                controller = controllers[controllerName] = new contructor(),
                result,
                viewName,
                lastIndexOfControllerName,
                lastIndexOfMethodName;

                if (typeof controller[methodName] !== 'function') {
                    methodName = 'indexMethod';
                }
                result = controller[methodName](req);

                lastIndexOfControllerName = controllerName.lastIndexOf('Controller');
                lastIndexOfControllerName = lastIndexOfControllerName === -1 ? 0 : lastIndexOfControllerName;

                lastIndexOfMethodName = methodName.lastIndexOf('Method');
                lastIndexOfMethodName = lastIndexOfMethodName === -1 ? 0 : lastIndexOfMethodName;


                viewName = 'pages/' +
                    controllerName.substr(0,lastIndexOfControllerName) +
                    '/' +
                    methodName.substr(0,lastIndexOfMethodName);

                viewFile = path.normalize(typeof result.viewFile !== 'undefined' ? viewPath + result.viewFile : viewPath + viewName + '.tpl');

                if (result.json && result.json === true) {
                    res.type('application/json');
                    res.send({'data': result.content});
                } else {
                    fs.exists(viewFile, function (exists) {
                        if (exists) {
                            res.render(viewFile, {'data': result});
                        } else {
                            res.status(500);
                            res.render('error', {'data': { 'viewName': viewFile} });
                        };
                    });
                };

        }
    };

module.exports = appController;