var defaultController = function () {
};

defaultController.prototype = {
    getData: function () {
        var fs = require('fs'),
            path = require('path'),
            cwd = process.cwd(),
            file = path.normalize(cwd + '/modell/dailymenu.json');

        return fs.readFileSync(file, 'utf8');
    },
    indexMethod: function (req, res) {
        var data = this.getData();
        var result = {
            "content": JSON.parse(data),
            "viewFile" : "pages/dailymenu_full_list.tpl"
        };
        return result;
    },

    test: function (req, res) {
        var result = {
        	"content": "testMethod @ defaultController",
        	"css" : "hello/test.css"
        };
        return result;
    },
    testjson: function (req, res) {
        var result = {
        	"json": true,
        	"content": "let's see if it works propery"
        };
        return result;
    }
};

module.exports = defaultController;