var helloController = function () {
};

helloController.prototype = {
    indexMethod: function (req, res) {
        var result = {
        	"content": "indexMethod @ helloController",
        	"css" : "hello/hello.css"
        };
        return result;
    },

    test: function (req, res) {
        var result = {
        	"content": "testMethod @ helloController",
        	"css" : "hello/test.css"
        };
        return result;
    },
    testjson: function (req, res) {
        var result = {
        	"json": true,
        	"content": "let's see if it works propery"
        };
        return result;
    }
};

module.exports = helloController;